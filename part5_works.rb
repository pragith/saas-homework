class Class
 
  def attr_accessor_with_history(attr_name)
	attr_name = attr_name.to_s
 
	class_eval "def #{attr_name}_history; @h_#{attr_name} = [ nil ] if not @h_#{attr_name}; @h_#{attr_name} end"
	class_eval "def #{attr_name}; @h_#{attr_name} = [ nil ] if not @h_#{attr_name}; @attr_name;  end"
	class_eval "def #{attr_name}=(val); @h_#{attr_name} = [ nil ] if not @h_#{attr_name};  @h_#{attr_name} << val; @attr_name = val; end"
  end
 
end