def palindrome?(string)
	string = string.gsub(/\W/,'').downcase
	if string.reverse == string
		return true
	else
		return false
	end
end
def count_words(string)
	string = string.downcase.gsub(/\W/,' ').split(" ")
	
	freqs = Hash.new(0)
	string.each { |s| freqs[s] += 1}
	freqs = freqs.sort_by {|x,y| y}
	freqs.reverse!
	
	hash = Hash.new(0)
	freqs.each {|word, freq| hash[word]=freq}
	
	return hash 
end