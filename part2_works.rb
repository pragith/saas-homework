class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end
 
def rps_game_winner(game)
  raise WrongNumberOfPlayersError unless game.length == 2
  player1, player2 = game
  combination = player1[1] + player2[1]
  raise NoSuchStrategyError unless combination =~ /[RSP]{2}/i
  return player1 if combination =~ /RR|RS|SS|SP|PP|PR/i
  return player2
end
 
def rps_tournament_winner(tournament)
  if tournament[0][0].kind_of?(Array)
	rps_game_winner([ rps_tournament_winner(tournament[0]), rps_tournament_winner(tournament[1]) ])
  else
	rps_game_winner(tournament)
  end
end