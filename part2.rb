class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end

def rps_game_winner(game)
	raise WrongNumberOfPlayersError unless game.length == 2
	
	p1 = {"name" => game[0][0],"choice" => game[0][1]}
	p2 = {"name" => game[1][0],"choice" => game[1][1]}
	
	x = p1["choice"].downcase
	y = p2["choice"].downcase
	
	
	raise NoSuchStrategyError unless (x == "r" or x == "s" or x == "p") and (y == "r" or y == "s" or y == "p")
	
	if ( (x=='s' and y=='p') or (x=='p' and y=='r') or (x=='r' and y=='s') or (x==y))
		return game[0]	
	else
		return game[1]
	end	
end